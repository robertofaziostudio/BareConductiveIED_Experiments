﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions; // Regular Expression

public class BareCondutiveReceived : MonoBehaviour 
{
	private GameObject _quad;
	public float mv = 0;
	public string raw_Data;
	public UnitySerialPort USP_Istance;

	string patternFindInt = @"\p{N}"; 	 // Save pattern decimal number
	string patternFindIntTwo = @"\d{2}"; // Save pattern with two decimal numbers
	Match[] matches = new Match[2]; 

	public bool checkMatch = true;
	public int cn = 11; // case reset to black


	public GameObject mongolfiera;
	public GameObject ventilatore;

	Renderer r_mong;
	Renderer r_vent;

	MovieTexture _MT_ventilatore;
	MovieTexture _MT_mongolfiera;

	void Start () 
	{
		r_mong = mongolfiera.GetComponent<Renderer>();
		r_vent = ventilatore.GetComponent<Renderer>();

		_MT_ventilatore = (MovieTexture)r_vent.material.mainTexture;
		_MT_mongolfiera = (MovieTexture)r_mong.material.mainTexture;

		mongolfiera.SetActive(false);
		ventilatore.SetActive(false);
	}

	void Update () 
	{

		// RawData from Serial Port
		raw_Data = USP_Istance.rData;
		Debug.Log(USP_Istance.RawData.ToString());

		FindAndMatch(checkMatch);
			
	}

	void FindAndMatch(bool checking)
	{	
		if(checking)
		{
			// Regex Class allows you to check the Regular Expression
			matches[0] = Regex.Match(raw_Data, patternFindInt);
			matches[1] = Regex.Match(raw_Data, patternFindIntTwo);

			if(matches[0].Success)
			{
				cn = int.Parse(matches[0].Value);
			}
			if(matches[1].Success)
			{
				cn = int.Parse(matches[1].Value);
			}
				
			SwitchCase();

		}
	
					
	}


	void SwitchCase()
	{
		switch (cn)
		{
		case 0:
			Debug.Log("Mongolfiera");
			mongolfiera.SetActive(true);
			_MT_mongolfiera.Play();
		
			break;

		case 1:
			Debug.Log("Ventilatore");
			ventilatore.SetActive(true);
			_MT_ventilatore.Play();
			break;

		case 2:
			Debug.Log("2");
			break;

		case 3:
			Debug.Log("3");
			break;

		case 4:
			Debug.Log("4");
			break;

		case 5:
			Debug.Log("5");
			break;

		case 6:
			Debug.Log("6");
			break;

		case 7:
			Debug.Log("7");
			break;

		case 8:
			Debug.Log("8");
			break;

		case 9:
			Debug.Log("9");
			break;

		case 10:
			Debug.Log("10");
			break;

		case 11:
			Debug.Log("11");
			break;

		case 12:
			Debug.Log("12 reset to Black");
			break;

		}

//		Quad_Array[cn].GetComponent<Renderer>().material.color = Color.red;

	}
}
